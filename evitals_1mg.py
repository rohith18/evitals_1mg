import json
import glob
import copy


def extract_evitals_data():
    with open("1p_skus.json") as file:
        evitals_data = json.load(file)
    return evitals_data


def extract_one_mg_data():
    files = glob.glob("./1mg/1mg_*/*/*.json")
    one_mg_data = []
    for file in files:
        with open(file) as file:
            one_mg_data += json.load(file)
    return one_mg_data


evitals_data = extract_evitals_data()
one_mg_data = extract_one_mg_data()


def update_evitals_data(evitals_data, one_mg_data):
    updated_evitals_data = []

    try:
        for item1 in evitals_data:
            for item2 in one_mg_data:
                if (item1.get("name") == item2.get("name") and item2.get("is_discontinued") == False):
                    item = copy.deepcopy(item1)
                    item["updatedMrp"] = item2.get("price")
                    item["type"] = item2.get("type")
                    updated_evitals_data.append(item)
    except AttributeError:
        pass

    return updated_evitals_data


updated_evitals_data = update_evitals_data(evitals_data, one_mg_data)

